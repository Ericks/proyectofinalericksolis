
var elementoBotonRegistrar = document.querySelector('#btnRegistrar');
elementoBotonRegistrar.addEventListener('click', registrar);

function registrar() {
  // obtiene la información del form
  var user = {
    firstName: document.getElementById('firstname').value,
    lastName: document.getElementById('lastname').value,
    address: document.getElementById('address').value,
    address2: document.getElementById('address2').value,
    country: document.getElementById('country').value,
    city: document.getElementById('city').value,
    correo: document.getElementById('correo').value,
    pass: document.getElementById('password').value,
  }
  
  var users = [];
  if (localStorage.getItem('users')) {
    users = JSON.parse(localStorage.getItem('users'));
  }
  users.push(user);
  localStorage.setItem('users', JSON.stringify(users));
  window.alert("Usuario registrado con éxito!");
  window.location.href = 'dasboard.html';

  clearFields();
}

function clearFields() {
  document.getElementById('firstname').value = '';
  document.getElementById('lastname').value = '';
  document.getElementById('address').value = '';
  document.getElementById('address2').value = '';
  document.getElementById('country').value = '';
  document.getElementById('city').value = '';
  document.getElementById('correo').value = '';
  document.getElementById('password').value = '';
  document.getElementById('firstname').focus();
}