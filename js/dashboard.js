var producto = [];
if (localStorage.getItem('producto')) {
    producto = JSON.parse(localStorage.getItem('producto'));
}

var UsuarioLogeado;
if (localStorage.getItem('UsuarioLogeado')) {
    UsuarioLogeado = JSON.parse(localStorage.getItem('UsuarioLogeado'));
}

function productos() {
    var row = document.getElementById("rows");

    row.innerHTML = '';

    for (let index = 0; index < producto.length; index++) {
        if (UsuarioLogeado.correo == producto[index].user.correo) {
            row.innerHTML += '<div class="d-flex flex-row col-lg-4 mb-4">' +
                '<a href="#" class="d-block">' +
                '<img class="img-fluid imgDash float-left mr-2" src="' + producto[index].url + '" alt="' + producto[index].producto + '">' +
                '</a>' +
                '<div class="d-flex flex-column p-2 w-100">' +
                '<a href="#" class="">' + producto[index].producto + ' </a>' +
                '<button type="button" name="' + producto[index].producto + '" class="btn btn-success btn-sm mb-3">Editar</button>' +
                '<button type="button" name="' + producto[index].producto + index + '"  class="btn btn-danger btn-sm">Eliminar</button>' +
                '</div>' +
                '</div>';
        }
    }
}

productos();


$(document).ready(function () {

    for (let index = 0; index < producto.length; index++) {
        console.log(producto[index].producto)
        $('button[name ="' + producto[index].producto + '"]').click(function () {
            //Editar
            localStorage.setItem('ProductoSeleccionado', JSON.stringify(producto[index]))
            var btrue = new Boolean(true);
            localStorage.setItem('editar/guardar', JSON.stringify(btrue))
            window.location.href = 'crear-editar.html';
        })

        $('button[name ="' + producto[index].producto + index + '"]').click(function () {
            var productoEliminar = producto.indexOf(producto[index]);
            producto.splice(productoEliminar, 1);
            localStorage.setItem('producto', JSON.stringify(producto));
            window.location.href = 'dasboard.html';
        })

    }

    $('#nuevo').click(function () {
        //boton nuevo
        var btrue = new Boolean(false);
        localStorage.setItem('editar/guardar', JSON.stringify(btrue))
        window.location.href = 'crear-editar.html';
    })

})



function nombre() {
    var nombre = document.getElementById("nombre");

    nombre.innerHTML = '';

    for (let index = 0; index < producto.length; index++) {
    }
    nombre.innerHTML += '<div class="texto-header">' +
        '<h1 class="titulo1">Dashboard</h1>' +
        '<p class="parrafo">Bienvenido <b>' + UsuarioLogeado.firstName + '</b>, aquí podrá el estado actual de sus<br> Cambalaches y sus productos registrados</p>' +
        '<hr class="linea1">' +
        '<a href="crear-editar.html" id="nuevo"><input type="submit" value="Nuevo" class="btn1"></a>' +
        '<hr class="linea2">' +
        '</div>';
}
nombre();

