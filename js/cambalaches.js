var producto = [];
if (localStorage.getItem('producto')) {
    producto = JSON.parse(localStorage.getItem('producto'));
}

function productos() {
    var row = document.getElementById("rows");

    row.innerHTML = '';

    for (let index = 0; index < producto.length; index++) {
        row.innerHTML += '<div class="d-flex flex-row col-lg-4 mb-4">' +
            '<a href="#" class="d-block">' +
            '<img class="img-fluid imgDash float-left mr-2" src="' + producto[index].url + '" alt="' + producto[index].producto + '">' +
            '</a>' +
            '<div class="d-flex flex-column p-2 w-100">' +
            '<h5>' + producto[index].producto +'</h5>'+
            '<p>' + producto[index].user.firstName +"."+ producto[index].user.lastName+'</p>'+
            '<button type="button" name="' + producto[index].producto + '" class="btn btn-success btn-sm mb-3">Detalle</button>' +
            '</div>' +
            '</div>';

    }
}

productos();


$(document).ready(function () {

    for (let index = 0; index < producto.length; index++) {
        $('button[name ="'+producto[index].producto+'"]').click(function(){
            //Seleccionar un producto
            localStorage.setItem('ProductoSeleccionado', JSON.stringify(producto[index]))
            window.location.href='detalleProducto.html';
        }) 

    }
})










