var elementoBotonGuardar = document.querySelector('#btnGuardar');
elementoBotonGuardar.addEventListener('click', guardar);

var editarGuardar;
if (localStorage.getItem('editar/guardar')) {
  editarGuardar = JSON.parse(localStorage.getItem('editar/guardar'));
}

var ProductoSeleccionado;
if (localStorage.getItem('ProductoSeleccionado')) {
  ProductoSeleccionado = JSON.parse(localStorage.getItem('ProductoSeleccionado'));
}


function guardar() {
  // obtiene la información del form
  var userLog;
  if (localStorage.getItem('UsuarioLogeado')) {
    userLog = JSON.parse(localStorage.getItem('UsuarioLogeado'));
  }

  var detalle = {
    producto: document.getElementById('txtNombreProducto').value,
    descripcion: document.getElementById('txtDescripcion').value,
    url: document.getElementById('txtURL').value,
    busco: document.getElementById('txtBusco').value,
    user: userLog,
  }

  var producto = [];
  if (localStorage.getItem('producto')) {
    producto = JSON.parse(localStorage.getItem('producto'));
  }

  if (editarGuardar == true) {
    editar();

  } else {
    producto.push(detalle);
    localStorage.setItem('producto', JSON.stringify(producto));
    window.alert("Producto Registrado/Editado con éxito!");
  }

  clearFields();

}

function clearFields() {
  document.getElementById('txtNombreProducto').value = '';
  document.getElementById('txtDescripcion').value = '';
  document.getElementById('txtURL').value = '';
  document.getElementById('txtBusco').value = '';
  document.getElementById('txtNombreProducto').focus();
}

//Editar producto
function cargarDatos() {


  if (editarGuardar == true) {
    document.getElementById('txtNombreProducto').value = ProductoSeleccionado.producto;
    document.getElementById('txtDescripcion').value = ProductoSeleccionado.descripcion;
    document.getElementById('txtURL').value = ProductoSeleccionado.url;
    document.getElementById('txtBusco').value = ProductoSeleccionado.busco;

  }

}




function editar() {
  var producto = [];
  if (localStorage.getItem('producto')) {
    producto = JSON.parse(localStorage.getItem('producto'));
  }

  for (var i = 0; i < producto.length; i++) {
    if (ProductoSeleccionado.producto == producto[i].producto) {

      producto[i].producto = document.getElementById('txtNombreProducto').value,
        producto[i].descripcion = document.getElementById('txtDescripcion').value,
        producto[i].url = document.getElementById('txtURL').value,
        producto[i].busco = document.getElementById('txtBusco').value,

        localStorage.setItem('producto', JSON.stringify(producto));
    }
  }
}

cargarDatos();








